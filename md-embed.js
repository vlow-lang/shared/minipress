const fs = require('fs')
const path = require('path');

module.exports = function (md) {
  const fileExists = f => {
    return fs.existsSync(f)
  }

  const readFileSync = f => {
    return fileExists(f) ? fs.readFileSync(f).toString() : `Not Found: ${f}`
  }

  const parseOptions = opts => {
    const _t = {}
    opts.trim().split(' ').forEach(pair => {
      const [opt, value] = pair.split('=')
      _t[opt] = value
    })
    return _t
  }

  const dataFactory = (state, pos, max) => {
    const _root = state.env.location
    const start = pos + 6
    const end = state.skipSpacesBack(max, pos) - 1
    const [opts, fullpathWithAtSym] = state.src.slice(start, end).trim().split('](')
    const fullpath = path.resolve(path.join(_root, fullpathWithAtSym))
    const pathParts = fullpath.split('/')
    const fileParts = pathParts[pathParts.length - 1].split('.')

    return {
      file: {
        resolve: fullpath,
        path: pathParts.slice(0, pathParts.length - 1).join('/'),
        name: fileParts.slice(0, fileParts.length - 1).join('.'),
        ext: fileParts[fileParts.length - 1]
      },
      options: parseOptions(opts),
      content: readFileSync(fullpath),
      fileExists: fileExists(fullpath)
    }
  }


  function parser (state, startLine, endLine, silent) {
    const matcher = [64, 91, 99, 111, 100, 101]
    const pos = state.bMarks[startLine] + state.tShift[startLine]
    const max = state.eMarks[startLine]

    if (state.sCount[startLine] - state.blkIndent >= 4) {
      return false
    }

    for (let i = 0; i < 6; ++i) {
      const ch = state.src.charCodeAt(pos + i)
      if (ch !== matcher[i] || pos + i >= max) return false
    }

    if (silent) return true

    // handle code snippet include
    const d = dataFactory(state, pos, max)

    const token = state.push('fence', 'code', 0)
    token.info = (d.options.lang || d.file.ext)
    token.content = d.fileExists && d.content
    token.markup = '```'
    token.map = [startLine, startLine + 1]

    state.line = startLine + 1
    return true
  }

  md.block.ruler.before('fence', 'snippet', parser)
}