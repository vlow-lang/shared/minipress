const container = require('markdown-it-container')

module.exports = function (md) {
  md.use(container, 'slot', {

    validate: function(params) {
      return params.trim().match(/^slot\s+(.*)$/);
    },

    render: function (tokens, idx) {
      const m = tokens[idx].info.trim().match(/^slot\s+(.*)$/);

      if (tokens[idx].nesting === 1) {
        return '<template v-slot:' + md.utils.escapeHtml(m[1]) + '>\n';
      } else {
        return '</template>\n';
      }
    }
  });
}