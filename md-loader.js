const globby = require('globby');
const { match } = require("path-to-regexp");
const { existsSync } = require('fs-extra')
const Url = require("url-parse");
const { dirname, resolve, join, sep, posix } = require("path");
const matter = require('gray-matter');

const md = require('markdown-it')({html: true});

md.use(require('./md-slotter.js'))
md.use(require('./md-embed.js'))

let config = {}

let path = resolve(".", "minipress.config.js")
try {
    if (existsSync(path)) {
        config = require(path)
        config.default({ md })
    }
} catch(err) {
    console.error(err)
}

const templates = globby.sync('./templates/**/*.vue');

const routes = templates.map(template => {
    let path = template
        .replace(/^\.\/templates/, "")
        .replace(/\.vue$/, "")
        .replace(/\[(.*?)\]/g, ":$1")
        .replace(/\/index$/, "") || "/"

    return {
        path: path ,
        test: match(path),
        component: template.replace(/^\./, "@")
    }
})

module.exports = function (markdown) {
    let path = this._module.resource
    const xx = join(process.cwd(), "pages")

    let relPath = path
        .substr(xx.length)
        .split(sep).join(posix.sep)
        .replace(/\?.*$/, "")
        .replace(/\.md$/, "")
        .replace(/\/index$/, "/")

    let route = routes.find(value => {
        return !!value.test(relPath)
    })

    if(!route) {
        console.error("cant find a layout for", relPath)
        process.exit(1)
    }

    this.cacheable();

    const data = matter(markdown, {excerpt_separator: '<!-- more -->'});


    const result = md.render(data.content, {location: dirname(resolve(path))});
    const excerpt = data.excerpt ? md.render(data.excerpt, {location: dirname(resolve(path))}) : "";

    let template = [
        '<template>',
        '<Layout :frontmatter="frontmatter">' + result + '</Layout>',
        '</template>',
    ].join("\n")

    let script = [
        '<script>',
        'import Layout from "'+ (route.component) +'";',
        'export const frontmatter = ' + JSON.stringify(data.data || {}) + ';',
        'export const excerpt = ' + JSON.stringify(excerpt) + ';',
        'export default {',
        '\tcomponents:{"Layout": Layout},',
        '\tdata () {',
        '\t\treturn {',
        '\t\t\tfrontmatter',
        '\t\t}',
        '\t}',
        '}',
        '</script>'
    ].join('\n')

    const {query} = new Url(path, true)

    if (query.vue !== undefined) {
        if (query.type === "template") {
            return template
        }
        if (query.type === "script") {
            return script
        }
    }

    return template + script
};
