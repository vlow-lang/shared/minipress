const globby = require("globby");
const { parse, resolve } = require("path");
const { existsSync } = require('fs-extra')

module.exports = function () {
    function createRoutesFor(files) {
        const imports = [];
        const routes = [];
        const meta = [];
        const urls = {};

        files.forEach((file, i) => {
            let url = file
                .replace(/^\.\/pages/, '')
                .replace(/\.md$/, '.html');

            let route = url.replace(/\/index.html$/, '/')

            imports.push(`import v_${i}, {frontmatter as v_${i}_frontmatter, excerpt as v_${i}_excerpt} from "${file}";`)
            routes.push(`\t{ path: "${route}", component: v_${i},}`)
            meta.push(`\t{ path: "${route}", props: {frontmatter: v_${i}_frontmatter, excerpt: v_${i}_excerpt, kartoffel: 43 }}`)
            urls[url] = route
        })

        const routing = [
            ...imports,
            "export let routes = [",
            routes.join(",\n"),
            "];",
            "export let urls = "+JSON.stringify(urls, null, 4),
            "export let pagemeta = [",
            meta.join(",\n"),
            "];"
        ].join("\n")

        return routing
    }

    const files = globby.sync('./pages/**/*.md');
    const routes = createRoutesFor(files);
    const result = routes
    return result;
}
