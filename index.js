const fs = require('fs-extra');
const path = require("path");

const pluginName = 'MinipressWebpackPlugin';

class MinipressWebpackPlugin {
    apply(compiler) {
        compiler.hooks.afterEmit.tapAsync(pluginName, (compilation) => {

            const dist = compilation.compiler.outputPath
            const bundle = path.resolve(dist, "bundle.js")

            const website = require(bundle).website

            const createAppWith = (context) => {
                return new Promise((resolve, reject) => {
                    const app = website.default()

                    // set server-side router's location
                    app.$router.push(context.url)

                    // wait until router has resolved possible async components and hooks
                    app.$router.onReady(() => {
                        const matchedComponents = app.$router.getMatchedComponents()
                        // no matched routes, reject with 404
                        if (!matchedComponents.length) {
                            return reject({ code: 404 })
                        }
                        // the Promise should resolve to the app instance so it can be rendered
                        resolve(app)
                    }, reject)
                })
            }

            const renderer = require('vue-server-renderer').createRenderer({
                template: require('fs').readFileSync('./index.production.html', 'utf-8')
            })

            for (const filename in website.urls) {
                let url = website.urls[filename];

                let meta = website.pagemeta.find(a => a.path == url).props

                createAppWith({ url })
                    .then(app => renderer.renderToString(app, meta))
                    .then(html => {
                        return fs.outputFile(`./dist/${filename}`, html)
                    })
                    .then(()=>{
                        console.log("prerendered", url)
                    })
                    .catch(err => {
                        console.error(err)
                        process.exit(1);
                    });
            }

        });
    }
}


MinipressWebpackPlugin.markdownLoader = require.resolve('./md-loader.js');
MinipressWebpackPlugin.routesLoader = require.resolve('./routes-loader.js');

module.exports = MinipressWebpackPlugin;
